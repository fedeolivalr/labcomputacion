Proceso min_a_horas
	Definir minutos, acumulador,horas Como Entero;
	acumulador<-0;	
	Repetir
		Escribir "Ingrese un tramo en minutos";
		Leer minutos;
		acumulador<-acumulador+minutos;
	Hasta Que minutos = 0;
	
	horas <-  trunc(acumulador/60);
	minutos <- acumulador - horas*60;
	
	Escribir "La cantidad total de horas es ", horas, ":", minutos;
FinProceso
