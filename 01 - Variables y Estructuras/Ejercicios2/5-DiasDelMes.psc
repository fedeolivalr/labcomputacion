Funcion es_bisiesto <- bisiesto(anio)	
	Definir es_bisiesto Como Logico;
	Escribir "Ingrese el a�o";
	Leer anio;
	es_bisiesto<-((anio%4=0 Y anio%100<>0) O anio%400=0);
	
FinFuncion


Proceso cantidadDias
	Definir mes Como Entero;
	Escribir "Ingrese el mes para saber la cantidad de dias";
	Leer mes;
	
	Segun mes Hacer
		1,3,5,7,8,10,12:
			Escribir "Tiene 31 dias";
		2:
			Definir anio Como Entero;
			Escribir "Ingrese el a�o";
			Leer anio;
			
			Si bisiesto(anio) Entonces
				Escribir "Tiene 29 dias";
				
			SiNo
				Escribir "Tiene 28 dias";
				
			FinSi
		4,6,9,11:
			Escribir "Tiene 30 dias";
		
		De Otro Modo:
			Escribir "El mes no existe";
	FinSegun
	
FinProceso
