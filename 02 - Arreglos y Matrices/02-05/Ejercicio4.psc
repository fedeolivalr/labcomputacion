Proceso palabraCortaLarga
	Definir palabras Como Caracter;
	Definir  i, iLarga, iCorta Como Entero;
	Dimension palabras[5];
	
	Para i<-0 hasta 4 Hacer
		Escribir "Ingrese una palabra ";
		Leer palabras[i]; 
	FinPara
	
	iLarga <- 0;
	iCorta <- 0;
	
	Para i<-1 hasta 4 Hacer		
		Si(Longitud(palabras[i]) > Longitud(palabras[iLarga])) Entonces
			iLarga <- i;			
		FinSi
		
		Si(Longitud(palabras[i]) < Longitud(palabras[iCorta])) Entonces
			iCorta <- i;			
		FinSi
	FinPara
	
	Escribir "La palabra mas larga es ", palabras[iLarga] ;
	Escribir "La palabra mas corta es ", palabras[iCorta] ;
	
FinProceso
