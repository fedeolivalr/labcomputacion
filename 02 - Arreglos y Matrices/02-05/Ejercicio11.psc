Proceso Ejercicio11
	Definir matriz,contadores, datoARemplazar, datoRemplazo, contiene,i,j,flag,aux, masRepetido Como Entero;
	Dimension matriz[10,10], contadores[21];
	flag<-0;
	//Carga
	Para i<-0 hasta 9 Hacer
		Para j<-0 hasta 9 Hacer
			matriz[i,j]<-Aleatorio(0,20);
			Escribir Sin Saltar "[",matriz[i,j],"]";
		FinPara
		Escribir "";
	FinPara
	Escribir "";
	
	
	//Verificar si el dato se encuentra
	Escribir "Ingrese el dato que quiere corrobar si se encuentra en la matriz";
	Leer contiene;
	Para i<-0 hasta 9 Hacer
		Para j<-0 hasta 9 Hacer
			Si matriz[i,j] = contiene Entonces
				Escribir "El dato ", contiene ," se encuentra en la posicion: [",i,",",j,"]";
				flag<-1;
			FinSi
		FinPara
	FinPara
	Si flag = 0 Entonces
		Escribir "El dato no se encuentra";
	FinSi
	
	//Remplazar un dato
	Escribir "Ingrese el dato a ser remplazado";
	Leer datoARemplazar;
	
	Escribir "Ingrese el dato por el cual desea remplazarlo";
	Leer datoRemplazo;
	
	Para i<-0 hasta 9 Hacer
		Para j<-0 hasta 9 Hacer
			Si matriz[i,j] = datoARemplazar Entonces
				matriz[i,j]<-datoRemplazo;
			FinSi
		FinPara
	FinPara
	
	//Numeros que mas se repite
	//inicializo contadores:
	Para i<-0 hasta 20 Hacer
		contadores[i]<-0;
	FinPara
	//cuento
	Para i<-0 hasta 9 Hacer
		Para j<-0 hasta 9 Hacer
			aux<-matriz[i,j];
			contadores[aux]<-contadores[aux]+1;
		FinPara
	FinPara
	
	//Verifico el mas repetido
	//Guardo la posicion de mas repetido
	masRepetido<-0;
	Para i<-1 hasta 20 Hacer
		Si contadores[i] > contadores[masRepetido] Entonces
			masRepetido<-i;
		FinSi
	FinPara
	
	//Nuestro el vector  de contadores y el el que mas se repite
	Escribir "Repeticiones de los datos:";
	Para i<-0 hasta 20 Hacer
		Escribir i,": ",contadores[i]," repeticiones";
	FinPara
	Escribir "El valor mas reptido es el ",masRepetido," y se repite ",contadores[masRepetido]," veces";
FinProceso
