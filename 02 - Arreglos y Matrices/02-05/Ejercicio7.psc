Proceso transpuesta
	Definir matriz,i,j Como Entero;
	Dimension matriz[3,3];
	
	Para  i<-0 Hasta 2 Hacer
		Para j<-0 Hasta 2 Hacer
			Escribir "Ingrese el valor [",i,",",j,"]";
;			Leer matriz[i,j];
		FinPara
	FinPara
	
	Escribir "Matriz original:";
	Para  i<-0 Hasta 2 Hacer
		Para j<-0 Hasta 2 Hacer
			Escribir Sin Saltar matriz[i,j]," ";
		FinPara
		Escribir "";
	FinPara
	
	Escribir "Matriz transpuesta";
	Para  i<-0 Hasta 2 Hacer
		Para j<-0 Hasta 2 Hacer
			Escribir Sin Saltar matriz[j,i]," ";
		FinPara
		Escribir "";
	FinPara
FinProceso
