Funcion MostrarMatriz(Matriz,filas,columnas)
	Definir i,j Como Entero;
	
	Para i<-0 Hasta filas-1 Hacer
		Para j<-0 hasta columnas-1 Hacer
			Escribir Sin Saltar"[ ",Matriz[i,j]," ]";
		FinPara
		Escribir "";
	FinPara
	
FinFuncion	
Proceso Ejercicio9
	Definir matriz1,matriz2,i,j,matrizResultado Como Entero;
	Dimension matriz1[5,5],matriz2[5,5],matrizResultado[5,5];
	
	//Cargo matrices 5x5 con numeros aleatorios entre -10 y 10
	Para i<-0 hasta 4 Hacer
		Para j<-0 hasta 4 Hacer
			matriz1[i,j]<-Aleatorio(-10,10);
			matriz2[i,j]<-Aleatorio(-10,10);
			matrizResultado[i,j]<- matriz1[i,j] + matriz2[i,j];
		FinPara
	FinPara

	MostrarMatriz(matriz1,5,5);
	Escribir "             +               ";
	MostrarMatriz(matriz2,5,5);
	Escribir "             =               ";
	MostrarMatriz(matrizResultado,5,5);
FinProceso
